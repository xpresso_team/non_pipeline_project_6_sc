# Calculator

class Calculator:
    """
    Calculator Function
    """
    def __init__(self, num_1, num_2):
        """
        Initialization
        :param num_1:
        :param num_2:
        """
        self.num_1 = num_1
        self.num_2 = num_2

    def add(self):
        """
        Addition of two numbers
        :return:
        """
        return self.num_1 + self.num_2


if __name__ == "__main__":

    num_1 = 5
    num_2 = 7
    calc = Calculator(num_1, num_2)
    print(calc.add())
